import { FC, ReactElement } from "react";

interface input {
  label: string;
  type: string;
  handleChange: any;
  value: string;
}

const Input: FC<input> = ({ label, type, handleChange, value }): ReactElement => {
  return (
    <div className="mb-6">
      <label
        htmlFor="default-input"
        className="text-left mb-2 font-sans font-medium text-gray-900 dark:text-gray-300"
      >
        {label}
      </label>
      <input
        type={type}
        onChange={(e) => handleChange(e, label)}
        value={value}
        id="default-input"
        className="bg-gray-50 border border-purple text-white text-sm rounded-l  block w-full p-2.5 dark:bg-gray-700 dark:border-purple dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
      />
    </div>
  );
};

export default Input;
