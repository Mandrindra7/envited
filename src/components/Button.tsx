import { FC, ReactElement } from "react";

interface button {
  title: string;
  onPress: any;
}
const Button: FC<button> = ({ title, onPress }): ReactElement => {
  return (
    <div>
      <button
        type="button"
        className="w-80 font-sans text-white bg-gradient-to-r from-purple to-pink hover:bg-gradient-to-l focus:ring-4 focus:outline-none focus:ring-purple-200 dark:focus:ring-purple-800 font-bold rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
        onClick={onPress}
      >
        {title}
      </button>
    </div>
  );
};

export default Button;
