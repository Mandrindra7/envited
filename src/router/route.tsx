import React from "react";
import { redirect } from "react-router";

import LandingPage from "../pages/LandingPage";
import AddEvent from "../pages/AddEvent";
import EventDetails from "../pages/EventDetails";

const routes = [
  {
    path: "/",
    redirect,
    element: <LandingPage />,
  },
  {
    path: "/add-event",
    element: <AddEvent />,
  },
  {
    path: "/event-details",
    element: <EventDetails />,
  },
];

export default routes;
