import image1 from "../assets/event_image.png"
export interface Event {
    event : string,
    host: string,
    startDate: string,
    endDate: string
    location: string,
    picture: string
}

export const data: Event = {
    event : "Birthday Bash" ,
    host: "Elysia",
    startDate: "18 August 6:00PM",
    endDate: "19 August 1:00PM UTC",
    location: "Suburb, State, Postcode",
    picture: image1
}