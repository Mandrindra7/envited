import { useState } from "react";
import { useNavigate } from "react-router";
import Button from "../components/Button";
import Input from "../components/Input";
import { Event } from "../data/Event";

const AddEvent = () => {
  const [data, setData] = useState<Event>({
    event: "",
    host: "",
    startDate: "",
    endDate: "",
    location: "",
    picture: "",
  });
  const navigate = useNavigate();
  const handleChange = (e: any, type: string) => {
    const _temp: Event = { ...data };
    _temp[type as keyof Event] = e.target.value;
    setData(_temp);
  };

  const createEvent = () => {
    localStorage.setItem("data", JSON.stringify(data));
    navigate("/event-details");
  };
  return (
    <div className="form">
      <h1 className="text-center font-bold font-sans text-darkpurple text-4xl">Add event</h1>
      <Input label={"event"} type={"text"} handleChange={handleChange} value={data["event"]} />
      <Input label={"host"} type={"text"} handleChange={handleChange} value={data["host"]} />
      <Input
        label={"startDate"}
        type={"date"}
        handleChange={handleChange}
        value={data["startDate"]}
      />
      <Input label={"endDate"} type={"date"} handleChange={handleChange} value={data["endDate"]} />
      <Input
        label={"location"}
        type={"text"}
        handleChange={handleChange}
        value={data["location"]}
      />
      <Input label={"picture"} type={"file"} handleChange={handleChange} value={data["picture"]} />
      <Button title="Add event" onPress={createEvent} />
    </div>
  );
};

export default AddEvent;
