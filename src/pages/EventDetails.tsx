import React, { useEffect, useState } from "react";
import { data } from "../data/Event";
import { Event } from "../data/Event";
import Calendar from "../assets/Calendar.svg";
import Location from "../assets/Location.svg";

const EventDetails = () => {
  const [event, setEvent] = useState<Event>(data);

  useEffect(() => {
    const res = localStorage.getItem("data");
    res && setEvent(JSON.parse(res));
  }, []);

  return (
    <div>
      <h1 className="text-center font-bold font-sans text-darkpurple text-4xl ">Event</h1>
      <div className="flex items-center justify-around">
        <div className="m-50">
          <h2 className="text-center font-bold font-sans text-darkpurple text-4xl mr-20">
            {event.event}
          </h2>
          <p className="font-sans font-medium text-gray text-left">
            Hosted by <span className="font-bold">{event.host}</span>
          </p>
          <div className="flex justify-between my-5">
            <span className="bg-white rounded-lg w-14 h-14 p-3">
              <img src={Calendar} />
            </span>
            <div>
              <p className="text-darkpurple font-bold text-left">{event.startDate}</p>
              <p className="text-gray font-light text-left">
                to <span className="text-darkgray font-bold">{event.endDate} UTC+ 10</span>
              </p>
            </div>
          </div>
          <div className="flex justify-between">
            <span className="bg-white rounded-lg  w-14 h-14 p-3">
              <img src={Location} />
            </span>
            <div className="">
              <p className="text-darkpurple font-sans font-bold text-left">Street name</p>
              <p className="text-gray font-light text-left">{event.location}</p>
            </div>
          </div>
        </div>
        <img className="mt-40" src={event.picture} />
      </div>
    </div>
  );
};

export default EventDetails;
