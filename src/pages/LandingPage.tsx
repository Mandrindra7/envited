import React from "react";
import { useNavigate } from "react-router";
import Button from "../components/Button";
import image1 from "../assets/image 1.png";

const LandingPage = () => {
  const navigate = useNavigate();
  return (
    <div className="flex justify-around">
      <img className="mr-80" src={image1} />
      <div className="ml-5 mt-60">
        <h1 className="text-6xl text-right text-darkpurple font-bold font-sans">Imagine if </h1>

        <h1 className="font-bold text-right text-transparent text-8xl bg-clip-text bg-gradient-to-br from-purple to-pink">
          Snapchat
        </h1>
        <h1 className="text-6xl text-right text-darkpurple font-bold">had events.</h1>
        <p className="text-right text-2xl font-light font-sans mb-5">
          Easily host and share events with your friends across any social media.
        </p>
        <div className="ml-80">
          <Button title={"🎉 Create my event"} onPress={() => navigate("/add-event")} />
        </div>
      </div>
    </div>
  );
};

export default LandingPage;
