import { BrowserRouter as Router, Routes,Route ,Outlet} from "react-router-dom";

import routes from "./router/route";
import './App.css'

function App() {

  return (
    <div className="App">
      <Router>
        <Routes>
            {routes.map((props, index) => <Route {...props} key={index} />)}
        </Routes>  
        <Outlet/>

      </Router>
    </div>
  )
}

export default App
