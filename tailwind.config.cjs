/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{ts,tsx}"],
  theme: {
    fontFamily: {
       'sans': ['Helvetica', 'Arial', 'sans-serif'],
    },
    extend: {},
    colors: {
      purple: '#8456EC',
      pink:'#E87BF8',
      darkpurple: '#240D57',
      lightpurple:'#501FC1',
      white: '#FBFAFF',
      gray:  '#828282',
      darkgray: '#4F4F4F'

    }
  },
  plugins: [],
}
